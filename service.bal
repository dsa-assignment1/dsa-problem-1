import ballerina/http;
import ballerina/io;


type learner record {#["username":codbops2,"lastname":Mason,"firstname":Alex,"preferred_formats":text,"course":Mathematics,"score":A+]
string username;
string lastname;
string firstname;
string preferred_formats;
string course;
string score;
};
type learning_materials record{#["courseName":"objectives":"topic":"audio":"video":"suggestedFormat":]
string courseName;
string objectives;
string topic;
string audio;
string video;
string suggestedFormat;
};
learning_materials [] learning_mat_det = [];
learner [] learnerDetails = [];
service /learnerService on new http:Listener(9016) {

resource function post createLearner(@http:Payload learner new_learner) returns json{
    io:println("Creating new learner");
    learnerDetails.push(new_learner);
    return {"Add":"learner added" + new_learner.username};
}
resource function get viewLearner() returns learner [] {
    io:println("Retrieving learner information...");
    return learnerDetails;
}
resource function put updateLearner(@http:Payload learner new_learner) returns json{
    io:println("Updating learner information");
    learnerDetails.push(new_learner);
    return {"Updated":"learner updated" + new_learner.username};
}
resource function post setupMaterials(@http:Payload learning_materials new_learnerMats) returns json{
    io:println("Setting up materials...");
    learning_mat_det.push(new_learnerMats);
    return {"Add":"materials established" + new_learnerMats.courseName};
}
resource function get viewMaterials() returns learning_materials [] {
 io:println("Retrieving materials...");
 return learning_mat_det;
}
}
